import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlayRoutingModule } from './play-routing.module';
import { PlayComponent } from './play/play.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
  imports: [CommonModule, PlayRoutingModule, MaterialModule],
  declarations: [PlayComponent]
})
export class PlayModule {}
