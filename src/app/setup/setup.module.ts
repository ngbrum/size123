import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SetupRoutingModule } from './setup-routing.module';
import { SetupComponent } from './setup/setup.component';
import { MaterialModule } from '../material/material.module';
@NgModule({
  imports: [CommonModule, SetupRoutingModule, MaterialModule],
  declarations: [SetupComponent]
})
export class SetupModule {}
