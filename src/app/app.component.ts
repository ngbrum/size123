import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  template: `
  <div class="holder">
    <router-outlet></router-outlet>
  </div>
  `,
  styles: [
    `
      .holder {
        background-color: #eee;
      }
    `
  ]
})
export class AppComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {
    const user = localStorage.getItem('user');
    if (!user) {
      return this.router.navigateByUrl('/setup');
    }
    return this.router.navigateByUrl('/play');
  }
}
